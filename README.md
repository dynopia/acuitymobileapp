# Acuity Mobile App #

![acuity128.png](https://bitbucket.org/repo/bMrnb8/images/1691266106-acuity128.png)


### What is this repository for? ###

* This repo serves as a very first (pre-alpha) stage of the Acuity mobile app in react-native. It currently features the onboarding screens.
* Version 0.0.1


### App preview: ##

You can download an iOS preview video file of the app through [this link](http://bit.ly/2cosq1S).

**OR**

You can download the app on your Android through [this link](http://bit.ly/2cP58Tp).

### How do I get set up? ###


* Step 1: Clone the repo: `git clone https://Jkok@bitbucket.org/dynopia/acuitymobileapp.git`
* Step 2: `npm install`
* Step 3: Run `npm run ios` or `npm run android`
* (Android only): If you debug on a real device:
* * make sure you shake the phone to access react-native properties
* * then set the Debug server host & port to your computers

ip i.e: 192.168.0.25 and port 8081 like so: `192.168.0.250:8081` otherwise you won't be able to make it work.

##### What we expect you to already have before debugging: #####
* If you wish to debug for iOS you must be on a Mac with the latest XCode and developer tools installed and configured.
* If you wish to debug for Android you must have the latest Android Studio and Android SDK installed and configured.


### Who do I talk to? ###

* For any questions contact: `sudoplz@gmail.com`!
