/**
 * # routingActions.js
 *
 * All the request actions have 3 variations, the request, a success
 * and a failure. They all follow the pattern that the request will
 * set the ```isFetching``` to true and the whether it's successful or
 * fails, setting it back to false.
 *
 */
'use strict';

import {ActionNames, Modals} from '../../config/constants';
/**
 * ## Imports
 *
 * The actions supported
 */
const {
  SET_MODAL_VISIBILITY,
  MANUAL_NAVIGATE_TO_PREVIOUS
} = ActionNames;

/**
* Import our router
*/
import {Actions} from 'react-native-router-flux';

/**
 * Project requirements
 */

// import {Actions} from 'react-native-router-flux';
// import _ from 'underscore';

// export function navigateState(schene) {
//   return {
//     type: NAVIGATE_TO,
//     payload: schene
//   };
// }
//
export function manualStateChangeNavigateToPrevious() {
  return {
    type: MANUAL_NAVIGATE_TO_PREVIOUS,
    payload: null
  };
}





/*
Action creators

*/

export function refreshCurrentShene(newProps) {
  return (dispatch, getState) => {
    // const state = getState()
    try{
      Actions.refresh(newProps);
    }catch(e){
      throw new Error("Schene: "+schene+ "nav error on refreshCurrentShene - : "+e);
    }
  }
}

export function navigateTo(schene, dataToTransferToNewShene, ableToNavigateToSelf = false) {
  return (dispatch, getState) => {
    const state = getState()

    if((ableToNavigateToSelf===false && state.router.currentSchene!=schene) || ableToNavigateToSelf===true){
      try{

        Actions[schene](dataToTransferToNewShene);
        return true;
      }catch(e){
        console.log("navigateTo error: "+JSON.stringify(e));
        throw new Error("Schene: "+schene+ "nav error: "+e);
        return false;
      }
    }else{
      console.log("We\'re already within "+schene);
      return false;
    }

  }
}


export function navigateToPrevious() {
    return (dispatch, getState) => {
      const state = getState()
      let prevSchenesList = state.router.previousSchenes;
      if(prevSchenesList!=null && prevSchenesList.size > 0){
        Actions.pop()
      }else{
        //do somethong when there is NO previous state.
        throw new Error("ERROR: No previous state to head to.")
      }
    }
}


export function setModalVisibility(modalName, visible) {
  return {
    type: SET_MODAL_VISIBILITY,
    payload: {name: modalName, visibility: visible}
  };
}
