/**
 * # globalActions.js
 *
 * Actions that are global in nature
 */
'use strict';

import {ActionNames} from '../../config/constants';

/**
 * ## Imports
 *
 * The actions supported
 */
const {
  SET_DEV,
  SET_STORE,
  SET_NAVBAR_DIMENSIONS,


} = ActionNames;


/**
 * ## set the store
 *
 * this is the Redux store
 *
 * this is here to support Hot Loading
 *
 */
export function setStore(store) {
  return {
    type: SET_STORE,
    payload: store
  };
}




/**
 * ## set the environment
 *
 */
export function setEnvironmentIsDev(environmentIsDev) {
  return {
    type: SET_DEV,
    payload: environmentIsDev
  };
}



export function setNavBarDimensions(dimensions){
  return {
    type: SET_NAVBAR_DIMENSIONS,
    payload: dimensions
  }
}
