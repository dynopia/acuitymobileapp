/**
 * # globalReducer.js
 *
 *
 */
'use strict';
import {ActionNames} from '../../config/constants';
/**
 * ## Imports
 * The InitialState for auth
 * fieldValidation for validating the fields
 * formValidation for setting the form's valid flag
 */
const {
  SET_DEV,
  SET_STORE,
  SET_NAVBAR_DIMENSIONS,

} = ActionNames;

import InitialState from './globalInitialState';

const initialState = new InitialState;
/**
 * ## globalReducer function
 * @param {Object} state - initialState
 * @param {Object} action - type and payload
 */
export default function globalReducer(state = initialState, action) {
  if (!(state instanceof InitialState)) return initialState.merge(state);

  switch (action.type) {

  case SET_DEV:
    return state.set('isDev', action.payload);

  case SET_NAVBAR_DIMENSIONS:
    return state.setIn(['navBarDimensions', 'width'],action.payload.width)
    .setIn(['navBarDimensions', 'height'],action.payload.height);
    break;



    /**
     * ### sets the payload into the store
     *
     * *Note* this is for support of Hot Loading - the payload is the
     * ```store``` itself.
     *
     */
  case SET_STORE:
    return state.set('store',action.payload);

  }

  return state;
}
