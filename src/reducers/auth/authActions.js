/**
 * # authActions.js
 *
 * All the request actions have 3 variations, the request, a success
 * and a failure. They all follow the pattern that the request will
 * set the ```isFetching``` to true and the whether it's successful or
 * fails, setting it back to false.
 *
 */
'use strict';


import {ActionNames, ScheneKeys, Modals} from '../../config/constants';

import moment from 'moment';
import {validateEmail, validatePassword} from '../../lib/Utils/fieldValidation';
import {timeout} from '../../lib/Utils/genericUtils';

import {setModalVisibility, navigateTo} from '../routing/routingActions'
import {Actions} from 'react-native-router-flux';

import AppAuthTokenStore from '../../lib/Storage/AppAuthTokenStore';
import UserInfoStore from '../../lib/Storage/UserInfoStore';


import _ from 'underscore';



/**
 * ## Imports
 *
 * The actions supported
 */
 /**
  * ## Auth actions
  */
 const {

   TOKEN_VALIDATE_REQUEST,
   TOKEN_VALIDATE_SUCCESS,
   TOKEN_VALIDATE_FAILURE,

   LOGOUT_REQUEST,
   LOGOUT_SUCCESS,
   LOGOUT_FAILURE,

   LOGIN_REQUEST,
   LOGIN_SUCCESS,
   LOGIN_FAILURE,

   LOGIN_FACEBOOK_REQUEST,
   LOGIN_FACEBOOK_SUCCESS,
   LOGIN_FACEBOOK_FAILURE,

   VALIDATE_REQUEST,
   VALIDATE_SUCCESS,
   VALIDATE_FAILURE,


   FORGOT_PASSWORD_REQUEST,
   FORGOT_PASSWORD_SUCCESS,
   FORGOT_PASSWORD_FAILURE,

   ON_AUTH_FORM_FIELD_CHANGE,


   SIGNUP_REQUEST,
   SIGNUP_SUCCESS,
   SIGNUP_FAILURE,

   SIGNUP_FACEBOOK_REQUEST,
   SIGNUP_FACEBOOK_SUCCESS,
   SIGNUP_FACEBOOK_FAILURE,

   RESET_ERROR_STATE,

   SET_STATE,
   SET_USER_DATA,
   SET_AUTH_METHOD,
   SET_PASSWORD_VISIBILITY,
   MANUALLY_INVOKED_FIELD_VALIDATION

 } = ActionNames;



const {
  WELCOME,
  FORGOT_PASSWORD,
} = Modals


//============================================================================//




/**
 * ## LOGOUT actions
 */
export function logoutSuccess() {
  return {
    type: LOGOUT_SUCCESS
  };
}




export function logout() {
  return async function(dispatch, getState){

      // console.log("Logged in using facebook? "+getState().auth.form.authMethod);
      // if(getState().auth.form.authMethod=="facebook"){
      //     console.log("Logging out from facebook")
      //     await LoginManager.logOut()
      // }
      deleteSessionTokenAndBasicInfo();
      dispatch(logoutSuccess());
      return dispatch(navigateTo(ScheneKeys.ONBOARDING, {type:'reset'}));
      // return res.data;
    // }
  }
}




/**
 * ## Signup actions
 */
export function signupRequest() {
  return {
    type: SIGNUP_REQUEST
  };
}
export function signupSuccess(json) {
  return {
    type: SIGNUP_SUCCESS,
    payload: json
  };
}
export function signupFailure(error) {
  return {
    type: SIGNUP_FAILURE,
    payload: error
  };
}
/**
 * ## signup
 * @param {string} username - name of user
 * @param {string} email - user's email
 * @param {string} password - user's password
 * @param {string} first_name - user's first_name
 * @param {string} last_name - user's last_name
 * @param {string} dayOfBirth - user's day of birth (timestamp)
 * @param {string} zipcode - user's zipcode
 * @param {string} topics - user's topics of interest (array of strings)
 * @param {string} gender - user's gender
 * @param {boolean} dev - (Optional) Wether our environment is dev or not
 *
 *
 * Otherwise, dispatch the error so the user can see
 */
export function signup(email, password, first_name, last_name, dayOfBirth, zipcode, topics, gender, dev=null) {
  return async function (dispatch){
    dispatch(signupRequest());
    await timeout(1000);
    var res = {data:{user_id: "aUserId", city: "userCity", address: "userAddress"}};

    if(!!res.error){
      dispatch(signupFailure("Signup error: "+res.error));
    }else{
      // console.log("Signup success");

      let userInfo = {user_id:res.data.user_id, email: email, first_name:first_name, last_name:last_name, city:res.data.city || res.data.address};
      saveSessionTokenAndBasicInfo(res.data.token, userInfo);
      dispatch(signupSuccess(Object.assign({}, res.data,
  			{
  			  email: email,
          first_name: first_name,
          last_name:last_name,
  			},
        userInfo
      )));
    }
    return dispatch(setModalVisibility(WELCOME, true));
  };
}




/**
 * ## FACEBOOK Signup actions
 */

export function facebookSignupRequest() {
  return {
    type: SIGNUP_FACEBOOK_REQUEST
  };
}

export function facebookSignupSuccess(data) {
  return {
    type: SIGNUP_FACEBOOK_SUCCESS,
    payload: data
  };
}

export function facebookSignupFailure(error) {
  return {
    type: SIGNUP_FACEBOOK_FAILURE,
    payload: error
  };
}

/**
 * ## Facebook signup
 *
 * We call this action to sign the user up using facebook oAuth
 *
 * @param {string} fbUserId - facebook user id
 * @param {string} fbUserToken - facebook user token
 * @param {string} imgUrl - user photo we fetch from facebook
 * @param {string} email - user's email
 * @param {string} firstName - user's first_name
 * @param {string} lastName - user's last_name
 * @param {string} dayOfBirth - user's day of birth (timestamp)
 * @param {string} zipCode - user's zipcode
 * @param {string} topics - user's topics of interest (array of strings)
 * @param {string} gender - user's gender
 * @param {boolean} dev - (Optional) Wether our environment is dev or not
 *
 */
 export function signupFacebook(fbUserId, fbUserToken, imgUrl, email, firstName, lastName, dayOfBirth, zipCode, topics, gender, dev=null) {
   return async function (dispatch){
     dispatch(facebookSignupRequest());
    //  console.log("dayOfBirth: "+dayOfBirth)
    await timeout(1000);
    var res = {data:{user_id: "aUserId", city: "userCity", address: "userAddress"}};

    //  console.log("RES: "+JSON.stringify(res));
     let curUser = null;
     if(!!res.error){
       dispatch(facebookSignupFailure("Facebook signup failed: "+res.error));
     }else{

        let userInfo = {user_id:res.data.user_id, email: email, first_name:firstName, last_name:lastName, city:res.data.city || res.data.address};
        curUser = Object.assign({}, res.data,
        {
            email: email,
            last_name:lastName,
            first_name: firstName
        },
        userInfo);

        saveSessionTokenAndBasicInfo(res.data.token, userInfo);
        dispatch(facebookSignupSuccess(curUser));
       }
       dispatch(setModalVisibility(WELCOME, true));
       return curUser;
   };
 }




/**
 * ## LOGIN actions
 */

export function loginRequest() {
  return {
    type: LOGIN_REQUEST
  };
}

export function loginSuccess(json) {
  return {
    type: LOGIN_SUCCESS,
    payload: json
  };
}

export function loginFailure(error) {
  return {
    type: LOGIN_FAILURE,
    payload: error
  };
}

/**
 * ## Login
 * @param {string} email - user's email
 * @param {string} password - user's password
 * @param {boolean} dev - (Optional) Wether our environment is dev or not
 *
 * After calling Backend, if response is good, save the json
 * which is the currentUser which contains the sessionToken
 *
 * If successful, set the state to logout
 * otherwise, dispatch a failure
 */

  export function login(email,  password, dev=null) {
    return async function(dispatch){

      let emailIsInvalid = validateEmail(email), passwordIsInvalid = validatePassword(password);

      if(emailIsInvalid || passwordIsInvalid){   //if any of the two credentials were invalid
        // console.log("emailIsInvalid"+emailIsInvalid+"passwordIsInvalid"+passwordIsInvalid);
        dispatch(loginFailure("Please provide us with a valid username and password."));
        return null;
      }else{ //if the login credentials given by the user were BOTH valid
        dispatch(loginRequest());
        await timeout(1000);
        var res = {data:{user_id:"aUserId", email:"aUserEmail", first_name:"aUserFirstName", last_name:"aUserLastName", city:"aUserCity", address:"aUserAddress"}};

        // console.log("RES: "+JSON.stringify(res));
        if(!!res.error){
          dispatch(loginFailure("Login error: "+res.error));
        }else{

          let userInfo = {user_id:res.data.user_id, email: res.data.email, first_name:res.data.first_name, last_name:res.data.last_name, city:res.data.city || res.data.address};
          saveSessionTokenAndBasicInfo(res.data.token, userInfo);
          // console.log("@@@@@@@@:::: "+JSON.stringify(userInfo))

          dispatch(loginSuccess(userInfo));
          return res.data;
        }
      }
  }
}




/**
 * ## Facebook Login actions
 */

export function facebookLoginRequest() {
  return {
    type: LOGIN_FACEBOOK_REQUEST
  };
}

export function facebookLoginSuccess(data) {
  return {
    type: LOGIN_FACEBOOK_SUCCESS,
    payload: data
  };
}

export function facebookLoginFailure(error) {
  return {
    type: LOGIN_FACEBOOK_FAILURE,
    payload: error
  };
}

export function loginFacebook(facebookUserId,  facebookAccessToken, dev=null) {
  return async function(dispatch){
    dispatch(facebookLoginRequest());
    await timeout(1000);
    var res = {data:{user_id:"aUserId", email:"aUserEmail", first_name:"aUserFirstName", last_name:"aUserLastName", city:"aUserCity", address:"aUserAddress"}};

    // console.log("Got res in authActions.login with error: "+res.error+" and data: "+res.data);
    // console.log("RES: "+JSON.stringify(res));
    if(!!res.error){
      alert("Facebook login error: "+res.error);
      return null;
    }else{
      // alert("Good that was right, the cake was a lie though..");
      // console.log(res.data.token);
      let userInfo = {user_id:res.data.user_id, email:res.data.email, first_name:res.data.first_name, last_name:res.data.last_name, city:res.data.city || res.data.address};
      console.log("@@@ FB "+res.data.user_id);
      saveSessionTokenAndBasicInfo(res.data.token, userInfo);

      dispatch(facebookLoginSuccess(userInfo));
      return res.data;
    }
}
}




/**
 * ## FORGOT_PASSWORD actions
 */

export function forgotPasswordRequest() {
  return {
    type: FORGOT_PASSWORD_REQUEST
  };
}

export function forgotPasswordSuccess() {
  return {
    type: FORGOT_PASSWORD_SUCCESS
  };
}

export function forgotPasswordFailure() {
  return {
    type: FORGOT_PASSWORD_FAILURE
  };
}

/**
 * ## Forgot password
 *
 * We call this action to send an email to the user to help him reset his password if he does not remember it.
 *
 * @param {string} email - user's email
 * @param {boolean} dev - (Optional) Wether our environment is dev or not
 *
 */
  export function forgotPassword(email, dev=null) {
    return async function(dispatch){
      dispatch(forgotPasswordRequest());

      await timeout(1000);
      var res = {};

      // console.log("Got res in authActions.forgotPassword with error: "+res.error+" and data: "+res.data);
      // console.log("RES: "+JSON.stringify(res));
      if(!!res.error){
        alert("We could not find an account with that email address.");
        dispatch(forgotPasswordFailure());
      }else{
        alert("In a real life scenario, at this point you should check your inbox for the reset password link.");
        dispatch(forgotPasswordSuccess());
      }
      return dispatch(setModalVisibility(FORGOT_PASSWORD, false));
  }
}





 /**
  * ## VALIDATE actions
  */

 export function validateRequest() {
   return {
     type: VALIDATE_REQUEST
   };
 }

 export function validateSuccess() {
   return {
     type: VALIDATE_SUCCESS
   };
 }

 export function validateFailure(error) {
   return {
     type: VALIDATE_FAILURE,
     payload: error
   };
 }
 /**
  * ## validateUserEmail
  *
  * We call this action to check wether the users email already exists in the backend
  *
  * @param {string} email - user's email to validate
  * @param {boolean} dev - (Optional) Wether our environment is dev or not
  *
  */
export function validateUserEmail(emailToValidate, dev=null){
  return async function (dispatch){
    dispatch(validateRequest());

    await timeout(1000);
    var res = {data:{}};

    // console.log("RES: "+JSON.stringify(res));
    if(!!res.error){
      dispatch(validateFailure("Theres already a user registered with this email address."));
    }else{
      // console.log(res.data.token);
      dispatch(validateSuccess(res.data));
      return res.data;

    }
  }
}




/**
 * ## Validate token actions
 */
export function validateTokenRequest() {
  return {
    type: TOKEN_VALIDATE_REQUEST
  };
}
export function validateTokenSuccess(json) {
  return {
    type: TOKEN_VALIDATE_SUCCESS,
    payload: json
  };
}
export function validateTokenFailure(error) {
  return {
    type: TOKEN_VALIDATE_FAILURE,
    payload: error
  };
}
/**
 * ## validateToken
 *
 * We call this action to check wether the user is already logged in (and thus his token has not expired on the server)
 *
 * @param {string} sessionToken - user's token to check if valid
 * @param {boolean} dev - (Optional) Wether our environment is dev or not
 *
 */
export function validateToken(sessionToken=null, dev = null) {
  return async function(dispatch){
    dispatch(validateTokenRequest());


    let tok = sessionToken;
    try{
        if(!sessionToken){
          let tk = await new AppAuthTokenStore().getOrReplaceSessionToken(sessionToken);
          tok = tk.sessionToken;
        }
    }catch(e){
      // console.log("Unable to fetch past token in authActions.validateToken() with error: "+e.message);
      dispatch(validateTokenFailure("No past token exists."));
      return null;
    }

    await timeout(1000);
    var res = {data:{}};

    // console.log("RES: "+JSON.stringify(res));
    if(!!res.error){

      try{  //delete expire token
        // console.log("Now deleting expired token.");
        new AppAuthTokenStore().deleteSessionToken();
      }catch(e){console.log("Unable to delete expired token: "+e.message);}

      // console.log("authActions.login :: Error msg: "+res.error)
      dispatch(validateTokenFailure(res.error));
      return null;

    }else{

      let userInfo = await new UserInfoStore().getOrReplaceUserInfo();
      // console.log("@@@@@@@@:::: USER info in persistence : "+JSON.stringify(userInfo))
      dispatch(validateTokenSuccess(res.data));
      dispatch(loginSuccess({
        user_id:userInfo.user_id || "",
        first_name:userInfo.first_name || "",
        email:userInfo.email || "",
        last_name:userInfo.last_name || "",
        city:userInfo.city || ""
      }));
      return res.data;
    }
}
}








//============================================================================//

/**
 * ## Various Util actions
 */


export function manuallyInvokeFieldValidationForScheme(schemeName){
  return {
    type: MANUALLY_INVOKED_FIELD_VALIDATION,
    payload: schemeName
  };
}



/*
  This function sets the authentication method.
  The value of the method can be EITHER  "facebook"  or "email".
*/
export function setAuthMethod(method) {
  return {
    type: SET_AUTH_METHOD,
    payload: method
  };
}


export function setUserData(userData){
  return {
    type: SET_USER_DATA,
    payload: userData
  }
}


export function setPasswordVisibility(isHidden){
 return {
   type: SET_PASSWORD_VISIBILITY,
   payload: !isHidden
 };
}


/**
 * ## onAuthFormFieldChange
 * Set the payload so the reducer can work on it
 */
export function onAuthFormFieldChange(field, value, scheneName) {
  return {
    type: ON_AUTH_FORM_FIELD_CHANGE,
    payload: {field: field, value: value, scheneName: scheneName}
  };
}



export function resetErrorState(){
  return {
    type: RESET_ERROR_STATE
  };
}




/**
 * ## saveSessionTokenAndBasicInfo
 * @param {Object} token - Our session token
 * @param {Object} basicInfo - An object that contains basic user info such as user_id, city, first_name
 */
export function saveSessionTokenAndBasicInfo(token, basicInfo=null) {

  if(basicInfo!=null){
    // console.log("NOW saving session token with basic info: "+JSON.stringify(basicInfo))
    new UserInfoStore().storeUserInfo(basicInfo);
  }
  return new AppAuthTokenStore().storeSessionToken(token);
}




/**
 * ## deleteSessionTokenAndBasicInfo
 */
export function deleteSessionTokenAndBasicInfo() {
  new UserInfoStore().deleteUserInfo();
  new AppAuthTokenStore().deleteSessionToken();
}




export function getBasicUserInfo(){
  return async function (dispatch, getState){
    let userInfo = ((await new UserInfoStore().getOrReplaceUserInfo()) || {user_id:"", first_name:"", last_name:"", email:"", city:""});

    return userInfo;
  }
}
