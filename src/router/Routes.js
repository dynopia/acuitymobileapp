import React from 'react';
import {StyleSheet} from 'react-native';

import {Colors, ScheneKeys} from '../config/constants';


/**
* ### Redux
*
*/
import {connect } from 'react-redux';


/*
*
* ### containers
*
* All the top level containers
*
*/
import EmailSignIn from '../containers/EmailSignIn';
import EmailSignUpStep1 from '../containers/EmailSignUpStep1';
import EmailSignUpStep2 from '../containers/EmailSignUpStep2';
import Onboarding from '../containers/Onboarding';
import Main from '../containers/Main';
import SplashScreen from '../containers/SplashScreen';
import NoInternetModal from '../components/Modals/NoInternetModal';


import TabIconFactory from '../containers/NavIcons/TabIconFactory';
import {Scene, Switch, Actions, Modal, Router} from 'react-native-router-flux';





const styles = StyleSheet.create({
  scene:{
    backgroundColor:'#F7F7F7'
  },
  navBar:{
    backgroundColor:Colors.primaryColor,
    borderBottomColor:Colors.transparentColor
  },
  tabBar:{
    flex:1,
    borderTopWidth:1,
    borderTopColor:"#E4E3E9",
    backgroundColor:'white'
  },
  iconContainerStyle:{
    // backgroundColor:'red',
    borderRightWidth:0.8,
    borderRightColor: "#e8e7ee",

  },
  tabBarSelectedItemStyle:{
    // , borderRightWidth:1
    backgroundColor:'transparent',
    borderBottomWidth:2,
    borderBottomColor:Colors.negativeAccentColor,
    // shadowColor: 'rgba(0, 0, 0, 0.12)',
    // shadowOpacity: 0.8,
    // shadowRadius: 2,
    // shadowOffset: {
    //   height: 1,
    //   width: 2,
    // },
  },
  tabScene:{
  },
  tabBarShadow:{
    backgroundColor:Colors.secondaryBorderColor,
    top:-1,
    height:1
  },

  title:{
    color:Colors.mainTextColor,
    fontFamily:"Whitney"
  },

  leftIcon:{
    tintColor: 'white'
  }
});

const defaultProps = {
   navigationBarStyle:styles.navBar,
   titleStyle:styles.title,
   leftButtonIconStyle:styles.leftIcon
};




//Connect w/ the Router
const RouterWithRedux = connect()(Router);

const acuityScenes = Actions.create(
  <Scene key="modal" component={Modal} >
    <Scene key={ScheneKeys.NO_INTERNET_MODAL} component={NoInternetModal} />
    <Scene key="root" hideNavBar={true} initial={true}>
    <Scene key={ScheneKeys.SPLASH_SCREEN} {...defaultProps} component={SplashScreen} type="replace" hideNavBar={true} initial={true}/>
    <Scene key={ScheneKeys.ONBOARDING} {...defaultProps} panHandlers={null} direction="vertical" component={Onboarding} type="push" hideNavBar={true}/>
      <Scene key={ScheneKeys.LOGIN} {...defaultProps} component={EmailSignIn} hideNavBar={false} title="Sign In" />
      <Scene key={ScheneKeys.REGISTER_STEP_1} {...defaultProps} title="Register" component={EmailSignUpStep1} hideNavBar={false} />
      <Scene key={ScheneKeys.REGISTER_STEP_2} {...defaultProps} title="Register" component={EmailSignUpStep2} hideNavBar={false} initial={false}/>
      <Scene key={ScheneKeys.MAIN} {...defaultProps} title="Main" component={Main} panHandlers={null}  hideNavBar={true} initial={false}/>
    </Scene>
  </Scene>
);

export default class Routes extends React.Component{

  render(){
    return <RouterWithRedux hideNavBar={false} sceneStyle={styles.scene} scenes={acuityScenes}/>;
  }
}
