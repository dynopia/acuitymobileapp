'use strict';

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  Keyboard
} from 'react-native';



class SPKeyboardAwareView extends React.Component {
  constructor(props) {
    super(props);
    this.subscriptions = null;
  }

  onKeyboardShown() {
    if(this.props.onKeyboardChange){
        this.props.onKeyboardChange(true)
    }
  }
  onKeyboardHidden(){
    if(this.props.onKeyboardChange){
        this.props.onKeyboardChange(false)
    }
  }

  componentWillMount() {
    this.subscriptions = [
      Keyboard.addListener('keyboardWillShow', this.onKeyboardShown.bind(this)),
      Keyboard.addListener('keyboardWillHide', this.onKeyboardHidden.bind(this)),
    ];
  }

  componentWillUnmount() {
    this.subscriptions.forEach((sub) => sub.remove());
  }





  /**
   * ### render
   * Setup some default presentations and render
   */
  render() {
    return(
      <KeyboardAvoidingView
      behavior='position'
      style={this.props.style}
      contentContainerStyle={this.props.contentContainerStyle}
      >
      {this.props.children}
      </KeyboardAvoidingView>
    );
  }
}



SPKeyboardAwareView.propTypes= {
  children: React.PropTypes.any,
  onKeyboardChange: React.PropTypes.func
};
export default SPKeyboardAwareView;
