import keyMirror from 'key-mirror';

module.exports =  {
  Colors: {
    primaryColor : "#4B9B95", //green
    secondaryColor: "#1B2738",  //dark blue
    accentColor: "#FFFFFF",   //white
    negativeAccentColor: "#DD5C5B", //pinkred
    alternativeAccentColor: '#ED9518',  //orange

    transparentColor: "rgba(0,0,0,0)",  //clear

    mainBorderColor: "rgba(0,0,0,0.2)", //very transparent black
    secondaryBorderColor: "rgba(0,0,0,0.12)", //very transparent black

    mainTextColor: "white",
    secondaryTextColor: "#B8B8BD",   //ultra light grey
    thirdTextColor: "#303030",  //almost black (dark grey)
    fourthTextColor: "#707070",   //grey
    fifthTextColor: "#4A4A4A",     //brownish grey
    sixthTextColor: "#707070CC",   //grey
    errorTextColor: "#a94442",    //red
    helpTextColor: "#999999",      //light grey



    titleBgColor: "#F6F5FF",   //greysh blue very light
    titleBgColorDark: "#E8E7EE", //grey very very light
    buttonBgColor: "#F3F3F3",  //blueish grey not so light

    evenRowBgColor: "#FFFFFF",
    oddRowBgColor: "#F1F0F7",
    evenRowBgHighlightColor: "#F2D8DC",
    oddRowBgHighlightColor: "#F2D8DC"

  },


  Modals: keyMirror({
    WELCOME: null,
    FORGOT_PASSWORD: null,
    GENDER_PICK: null,
    DATE_PICK: null,
  }),

  ScheneKeys: keyMirror({
    SPLASH_SCREEN:null,
    ONBOARDING: null,
    LOGIN: null,
    REGISTER_STEP_1: null,
    REGISTER_STEP_1_FB: null,
    REGISTER_STEP_2: null,
    NO_INTERNET_MODAL: null,
    MAIN: null
  }),
  StorageKeys:{
    SESSION_TOKEN_STORAGE_KEY: "token",
    USER_INFO_STORAGE_KEY: "userInfo",
  },


  ActionNames:  keyMirror({

   /* global */
   SET_NAVBAR_DIMENSIONS: null,
   SET_STORE: null,


   /* device */
   SET_DEV: null,
   SET_PLATFORM: null,
   SET_VERSION: null,
   SET_ORIENTATION: null,
   SET_ORIENTATION_LOCK:null,


   /* Auth */
   SET_AUTH_METHOD: null,
   SET_SESSION_TOKEN: null,
   SET_USER_DATA: null,
   SET_PASSWORD_VISIBILITY:null,
   RESET_ERROR_STATE:null,
   MANUALLY_INVOKED_FIELD_VALIDATION: null,

   ON_AUTH_FORM_FIELD_CHANGE:null,

   TOKEN_VALIDATE_REQUEST: null,
   TOKEN_VALIDATE_SUCCESS: null,
   TOKEN_VALIDATE_FAILURE: null,

   SIGNUP_REQUEST: null,
   SIGNUP_SUCCESS: null,
   SIGNUP_FAILURE: null,

   SIGNUP_FACEBOOK_REQUEST: null,
   SIGNUP_FACEBOOK_SUCCESS: null,
   SIGNUP_FACEBOOK_FAILURE: null,

   VALIDATE_REQUEST: null,
   VALIDATE_SUCCESS: null,
   VALIDATE_FAILURE: null,

   LOGIN_REQUEST: null,
   LOGIN_SUCCESS: null,
   LOGIN_FAILURE: null,

   LOGIN_FACEBOOK_REQUEST:null,
   LOGIN_FACEBOOK_SUCCESS:null,
   LOGIN_FACEBOOK_FAILURE:null,

   FORGOT_PASSWORD_REQUEST: null,
   FORGOT_PASSWORD_SUCCESS: null,
   FORGOT_PASSWORD_FAILURE: null,

   FACEBOOK_DATA_ACQ_REQUEST: null,
   FACEBOOK_DATA_ACQ_SUCCESS: null,
   FACEBOOK_DATA_ACQ_FAILURE: null,


   /* Routing */
   SET_MODAL_VISIBILITY: null,
   MANUAL_NAVIGATE_TO_PREVIOUS:null,



   LOGOUT_REQUEST: null,
   LOGOUT_SUCCESS: null,
   LOGOUT_FAILURE: null,

 })

};
