import {version} from '../../package.json'
module.exports = {
  VERSION: version,
  ENVIRONMENT_IS_DEV: true,

  MOCK_TOKEN: false,  //set to true to actually mock the token with the ones below, false means we use the real retrieved token. If true, you must set the values below
  DEV_TOKEN: "",
  PROD_TOKEN: ""
}
