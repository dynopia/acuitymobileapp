/* @flow */
/**
 * # MainRender.js
 *
 * This class is a little complicated as it handles multiple states.
 *
 */
'use strict';


import Button from 'sp-react-native-iconbutton';
import {Colors, ScheneKeys} from '../../config/constants';
import React from 'react';
import {StyleSheet, Text, View, Platform} from 'react-native';
import {getCorrectFontSizeForScreen} from '../../lib/Utils/multiResolution'
import Dimensions from 'Dimensions';
const {height:h, width:w} = Dimensions.get('window'); // Screen dimensions in current orientation


const styles = StyleSheet.create({


  container: {
    flex:1,
    flexDirection:'column',
    backgroundColor:Colors.primaryColor,
    justifyContent:'center'
  },

  titleText:{
    color:Colors.mainTextColor,
    fontSize: getCorrectFontSizeForScreen(15),
    fontFamily: 'Whitney-Bold',
    textAlign: 'center',
  },
  logoutBtn:{
    marginHorizontal:w*0.05,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: Colors.mainTextColor,
    marginTop:h*0.02,
    height: 45
  },
  logoutBtnText:{
    color: Colors.mainTextColor,
    textAlign: 'center',
    fontFamily: 'Whitney-SemiBold',
    fontSize: getCorrectFontSizeForScreen(11),
  }

});






class MainRender extends React.Component {
  constructor(props) {
    super(props);
  }



  /**
   * ### render
   * Setup some default presentations and render
   */
  render() {

    return(
      <View style={styles.container}>
        <Text style={styles.titleText}>
        Main screen
        </Text>
        <Button
        onPress={this.props.onLogoutBtnPress}
        style={styles.logoutBtn}
        textStyle={styles.logoutBtnText}
        >
          Logout
        </Button>
      </View>
    );
  }



}


export default MainRender;
