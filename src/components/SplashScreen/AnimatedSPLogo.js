/* @flow */
/**
 * # AnimatedSPLogo.js
 *
 * This class is a little complicated as it handles multiple states.
 *
 */
'use strict';



import SPSpinner from '../../lib/UI/SPSpinner'


import {Colors, ScheneKeys} from '../../config/constants';

import React from 'react';
import {StyleSheet, Image, Text, View,Animated, Platform, Easing} from 'react-native';
import {getCorrectFontSizeForScreen} from '../../lib/Utils/multiResolution'
import Dimensions from 'Dimensions';
const {height:h, width:w} = Dimensions.get('window'); // Screen dimensions in current orientation

import accuityLogoBG from '../../../assets/logoAnimBg.png';
import accuityLogoPointerBig from '../../../assets/logoAnimPB.png';
import accuityLogoPointerSmall from '../../../assets/logoAnimPS.png';



const ANIMATION_PARTS = 10;  //that we will divide the 360 deegrees with
const ANIMATION_DURATION = 120; //in millisec
const PAUSE_DURATION = 100; //in millisec



const styles = StyleSheet.create({

  container:{
    alignItems:'center',
    padding: h*0.019,
    // backgroundColor: "pink",
  },
  accuityImageContainer:{
      width:h*0.15,
      height:h*0.15
  },
  accuityLogoImg:{
    position: 'absolute',
    width:h*0.15,
    height:h*0.15
  }

});

class AnimatedSPLogo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      animationSmall: new Animated.Value(0),
      animationBig: new Animated.Value(0),
      animationPart: 1,
      shouldStop: false,
      hasStarted: false
    };
    this.next = ()=>{};
  }













  animate(){
    if(this.state.shouldStop===false){
      Animated.parallel([
        Animated.timing(       // Uses easing functions
          this.state.animationSmall, // The value to drive
          {
            toValue: this.state.animationPart/(ANIMATION_PARTS*12),        // Target
            duration: ANIMATION_DURATION,    // Configuration
            delay:PAUSE_DURATION,
            easing: Easing.ease
          },
        ),
        Animated.timing(       // Uses easing functions
          this.state.animationBig, // The value to drive
          {
            toValue: this.state.animationPart/ANIMATION_PARTS,        // Target
            duration: ANIMATION_DURATION,    // Configuration
            delay:PAUSE_DURATION,
            easing: Easing.ease
          },
        )
      ]).start(
        ()=>{
          let newAnimationPart = this.state.animationPart+1.5;
          this.setState({
            animationPart: newAnimationPart
          })
          this.next();
        }
      );             // Don't forget start!
    }
  }

  startAnimating(){

    if(this.state.hasStarted===false){
      this.next = this.animate;
      this.next();
      this.setState({
        shouldStop: false,
        hasStarted: true
      })

    }
  }
  stopAnimating(){
    this.setState({
      shouldStop: true,
      hasStarted: false
    })
    this.next = ()=>{};
  }

  componentDidMount() {
    // this.animate();
  }










  /**
   * ### render
   * Setup some default presentations and render
   */
  render() {

    return(
      <View style={[styles.container, this.props.style]}>

        <View style={styles.accuityImageContainer}>
          <Image
          style={styles.accuityLogoImg}
          source={accuityLogoBG}
          resizeMode='contain'
          />
          <Animated.Image
            style={[styles.accuityLogoImg,
            {
              transform: [   // Array order matters
                {rotate:this.state.animationBig.interpolate({
                  inputRange: [0, 1],
                  outputRange: [
                    '0deg', '360deg' // 'deg' or 'rad'
                  ],
                })},
              ]
              // transform: [{scaleX: this.state.curLine1Width}]
            }]}
            source={accuityLogoPointerBig}
            resizeMode='contain'
          />
          <Animated.Image
            style={[styles.accuityLogoImg,
            {
              transform: [   // Array order matters
                {rotate:this.state.animationSmall.interpolate({
                  inputRange: [0, 1],
                  outputRange: [
                    '0deg', '360deg' // 'deg' or 'rad'
                  ],
                })},
              ]
              // transform: [{scaleX: this.state.curLine1Width}]
            }]}
            source={accuityLogoPointerSmall}
            resizeMode='contain'
          />
        </View>
      </View>
    );
  }

}


export default AnimatedSPLogo;
