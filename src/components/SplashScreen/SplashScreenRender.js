/* @flow */
/**
 * # SplashScreenRender.js
 *
 * This class is a little complicated as it handles multiple states.
 *
 */
'use strict';


import LinearGradient from 'react-native-linear-gradient';

import SPSpinner from '../../lib/UI/SPSpinner'


import {Colors, ScheneKeys} from '../../config/constants';

import React from 'react';
import {StyleSheet, Text, View, Platform, StatusBar} from 'react-native';
import {getCorrectFontSizeForScreen} from '../../lib/Utils/multiResolution'
import Dimensions from 'Dimensions';
const {height:h, width:w} = Dimensions.get('window'); // Screen dimensions in current orientation

import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icomoonConfig from '../../../assets/fonts/icomoon.json';

// const SPIcon = createIconSetFromIcoMoon(icomoonConfig);

import AnimatedSPLogo from './AnimatedSPLogo';






const styles = StyleSheet.create({


  container: {
    // backgroundColor: 'orange',
    flex:1,
    flexDirection:'column',
    justifyContent:'space-around',
    // backgroundColor: '#E8E7EE',
    // marginVertical: 10,
    // marginHorizontal:15
  },
  accuityLogoContainer:{
    // flex:1,
    // paddingVertical:h*0.08,
    justifyContent:'flex-start',
    // backgroundColor: "pink",
  },
  logoAnimation:{
    // backgroundColor: "purple",
    paddingVertical:h*0.08,
  },


  titleTextContainer:{
    justifyContent:'center',
    paddingVertical:h*0.14,
    backgroundColor:Colors.transparentColor
    // backgroundColor:'red'
  },
  titleText: {
    // backgroundColor: 'black',
    fontSize: getCorrectFontSizeForScreen(31),
    color: Colors.mainTextColor,
    fontFamily: 'Whitney-Bold',
    textAlign: 'center',
  },
  titleSubtext:{
    fontSize: getCorrectFontSizeForScreen(18),
    color: Colors.mainTextColor,
    fontFamily: 'Whitney-Light',
    textAlign: 'center',
  }

  // accountSettingsText:{
  //   color: Colors.primaryColor,
  //   textAlign: 'left',
  //   fontFamily: 'Whitney-Regular',
  //   fontSize: getCorrectFontSizeForScreen(10),
  // }

});






class SplashScreenRender extends React.Component {
  constructor(props) {
    super(props);
  }



  /**
   * ### render
   * Setup some default presentations and render
   */
  render() {

    // let isPortrait = (this.props.orientation!="LANDSCAPE");
    // console.log("@@@@ IS PORTRAIT : "+isPortrait);
    // let styles= isPortrait?this.getPortraitStyles(this):this.getLandscapeStyles(this);
    // '#4D6EB2',

    return(
      <LinearGradient
        colors={[ '#469FA5',  '#5CB8BA']}
        start={[0.5, 0.0]} end={[0.5, 1.0]}
        style={styles.container}
      >
        <StatusBar
           backgroundColor="#2f6763"
           translucent={false}
           barStyle="light-content"
        />
        <View style={styles.accuityLogoContainer}>
          <AnimatedSPLogo style={styles.logoAnimation} ref='accuityAnimatedLogo'/>
        </View>
        <View style={styles.titleTextContainer}>
          <Text style={styles.titleText}>Acuity</Text>
          <Text style={styles.titleSubtext}>Your personal secretary.</Text>
        </View>

      </LinearGradient>
    );
  }

  componentDidMount(){
    this.refs.accuityAnimatedLogo.startAnimating();
  }
  componentWillUnmount(){
    this.refs.accuityAnimatedLogo.stopAnimating();
  }

}


export default SplashScreenRender;
