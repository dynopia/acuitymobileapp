/**
 * Main.js
 *
 * Allow user to register
 */
'use strict';
/**
 * ## Imports
 *
 * Redux
 */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

/**
 * The actions we need
 */
import * as authActions from '../reducers/auth/authActions';
// import * as routingActions from '../reducers/routing/routingActions';
// import * as deviceActions from '../reducers/device/deviceActions';


/**
 * Router actions
 */
import { Actions } from 'react-native-router-flux';

/**
 * Immutable
 */
import {Map} from 'immutable';

/**
 *   LoginRender
 */
// import LoginRender from '../components/LoginRender';
import MainRender from '../components/Main/MainRender';
import React from 'react';


/**
 * ## Redux boilerplate
 */
const actions = [
  authActions,
  // routingActions,
  // deviceActions
];

function mapStateToProps(state) {
  return {
      ...state
  };
}

function mapDispatchToProps(dispatch) {
  const creators = Map()
          .merge(...actions)
          .filter(value => typeof value === 'function')
          .toObject();

  return {
    actions: bindActionCreators(creators, dispatch),
    dispatch
  };
}






class Main extends React.Component{

  onLogoutBtnPressed(){
    this.props.actions.logout();
  }

  render() {

    return(
      <MainRender
          onLogoutBtnPress={ this.onLogoutBtnPressed.bind(this) }
      />

    );
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(Main);
